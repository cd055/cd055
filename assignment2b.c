#include <stdio.h>

float input()
{
    float a;
    printf("Enter the value:\n");
    scanf("%f",&a);
    return a;
}
float area(float b,float h)
{
    float area=0.5*b*h;
    return area;
}
void display(float area)
{
    printf("The area of the triangle is %f",area);
}
int main()
{
    float b,h,area;
    b=input();
    h=input();
    area=area(b,h);
    display(area);
    return 0;
}