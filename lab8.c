#include<stdio.h>
int main()
{
	struct employee
	{
		int emp_num;
		char emp_name[60];
		float salary;
		char DOB[60];
		char company_name[60];
	};
	struct employee e1;
	printf("Enter the employee number:\n");
	scanf("%d",&e1.emp_num);
	printf("Enter the Employee name:\n");
	scanf("%s",e1.emp_name);
	printf("Enter the salary:\n");
	scanf("%f",&e1.salary);
	printf("Enter the DOB:\n");
	scanf("%s",e1.DOB);
	printf("Enter the company name:\n");
	scanf("%s",e1.company_name);
	printf("\n \nEMPLOYEE'S DETAILS:\n");
	printf("Employee number =%d\n",e1.emp_num);
	printf("Employee name =%s\n",e1.emp_name);
	printf("Employee salary =%0.2f\n",e1.salary);
	printf("Employee DOB =%s\n",e1.DOB);
	printf("Company name =%s\n",e1.company_name);
	return 0;
}
