#include <stdio.h>
float input()
{
	float a;
	printf("Enter the radius of circle:\n");
	scanf("%f",&a);
	return a;
}
float area(float a)
{
	float area=3.14*a*a;
	return area;
}
void display(float area)
{
	printf("The area of the circle is %f",area);
}
int main()
{
	float r,area;
	r=input();
	area=area(r);
	display(area);
	return 0;
}

