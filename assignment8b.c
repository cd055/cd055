#include<stdio.h>
#include<math.h>
int main()
{
    int i=0,decimal,remainder;
    double binary_num=0;
    printf("Enter decimal number");
    scanf("%d",&decimal);
    do
    {
        remainder=decimal%2;
        binary_num=binary_num+(remainder*pow(10,i));
        decimal=decimal/2;
        i++;
    }
    while(decimal!=0);
    printf("\nBinary number equivalent of the entered number is:%lf",binary_num);
    return 0;
}