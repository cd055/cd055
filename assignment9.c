#include <stdio.h>
int main()
{
	int array[2][3];
	int i,j;
	for(i=0;i<2;i++)
	{
		for(j=0;j<3;j++)
		{
			printf("Enter value for array[%d][%d]:",i,j);
			scanf("%d",&array[i][j]);
		}
	}
	printf("The 2 dimensional array elements are:");
	for(i=0;i<2;i++)
	{
		printf("\n");
		for(j=0;j<3;j++)
		{
			printf("\t %d",array[i][j]);
		}
	}
	return 0;
}
