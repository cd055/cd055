#include<stdio.h>

struct student
{
char first_name[25];
char last_name[25];
int roll_num;
char department[3];
float fees;
char section;
int total_marks;
};

int main()
{
struct student s[2];
int i;
for(i=0;i<2;i++)
{
printf("\nEnter the details of the student %d:\n\n",i+1);
printf("\nEnter the roll number:\n");
scanf("%d",&s[i].roll_num);
printf("Enter the first name:\n");
scanf("%s",&s[i].first_name);
printf("Enter the last name:\n");
scanf("%s",&s[i].last_name);
printf("Enter the section:\n");
scanf("%s",&s[i].section);
printf("Enter the department:\n");
scanf("%s",&s[i].department);
printf("Enter the fees:\n");
scanf("%f",&s[i].fees);
printf("Enter the total marks obtained out of 500:\n");
scanf("%d",&s[i].total_marks);
}
printf("\n\nSTUDENT'S DETAILS WHO HAS SCORED HIGHEST MARKS ARE:\n\n");
if(s[1].total_marks>s[2].total_marks)
{

    printf("Roll number=%d\n",s[1].roll_num);
    printf("First name=%s\n",s[1].first_name);
    printf("Last name=%s\n",s[1].last_name);
    printf("Section=%s\n",s[1].section);
    printf("Department=%s\n",s[1].department);
    printf("Total marks=%d\n",s[1].total_marks);
}

else
{
    printf("Roll number=%d\n",s[2].roll_num);
    printf("First name=%s\n",s[2].first_name);
    printf("Last name=%s\n",s[2].last_name);
    printf("Section=%s\n",s[2].section);
    printf("Department=%s\n",s[2].department);
    printf("Total marks=%d\n",s[2].total_marks);
}
return 0;
}
