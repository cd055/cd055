#include<stdio.h>
void add(int *a, int *b, int *s);
void div(int *a, int *b, int *d);
void rem(int *a, int *b, int *r);
void mul(int *a, int *b, int *m);
int main()
{
	int num1,num2,sum,divi,rema,mult;
	printf("Enter 2 numbers:\n");
	scanf("%d%d",&num1,&num2);
	add(&num1,&num2,&sum);
	printf("Sum of two numbers is %d\n",sum);
    div(&num1,&num2,&divi);
	printf("Division of two numbers is %d\n",divi);
	rem(&num1,&num2,&rema);
	printf("Remainder of two numbers is %d\n",rema);
	mul(&num1,&num2,&mult);
	printf("Multiplication of two numbers is %d\n",mult);
	return 0;
}
void add(int *a, int *b, int *s)
{
	*s=*a+*b;
}
void div(int *a, int *b, int *d)
{
	*d=(*a)/(*b);
}
void rem(int *a, int *b, int *r)
{
	*r=(*a)%(*b);
}
void mul(int *a, int *b, int *m)
{
	*m=(*a)*(*b);
}
