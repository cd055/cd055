#include <stdio.h>
#include<math.h>
int main()
{
	int a,b,c;
	float d,r1,r2;
	printf("Enter the co-efficients of x^2, x and value of constant");
	scanf("%d%d%d",&a,&b,&c);
	d=(b*b)-(4*a*c);
	if(d>0)
	{
		r1=(-b+sqrt(d))/(2*a);
		r2=(-b-sqrt(d))/(2*a);
		printf("The two real and distinct roots of the quadratic equation are %f %f",r1,r2);
	}
	else if(d==0)
	{
		r1=(-b)/(2*a);
		printf("The two real and equal roots of the quadratic equation are %f",r1);
	}
	else(d<0)
	{
		printf("The quadratic equation has imaginary roots");
	}
}