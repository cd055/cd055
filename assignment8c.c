#include <stdio.h>
int main()
{
    int n,rev_num=0,remainder,og_num;
    printf("Enter a number:");
    scanf("%d",&n);
    og_num=n;
    while(n!=0)
    {
        remainder=n%10;
        rev_num=rev_num*10+remainder;
        n=n/10;
    }
    if(og_num==rev_num)
        printf("The number entered is palindrome");
    else
        printf("The number is not a palindrome");
    return 0;
}